/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import Model.Mahasiswa;
import Model.Penduduk;

/**
 *
 * @author ASUS
 */
public class DataMahasiswa extends JDialog implements ActionListener {

    private JTable jt;
    private JScrollPane sp;
    private Mahasiswa mhs;
    private JLabel label_title;
    private JLabel label_nim;
    private JLabel label_nama;
    private JLabel label_ttl;
    private JTextField text_nim;
    private JTextField text_nama;
    private JTextField text_ttl;
    private JTextArea area_nama;

    public DataMahasiswa() {
        input();
    }

    public void input() {
        this.setLayout(null);

        label_title = new JLabel("List Mahasiswa");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);

        label_nama = new JLabel("Nama\t :");
        label_nama.setBounds(80, 60, 100, 50);
        this.add(label_nama);
        String a = "";
        System.out.println(Main.anggota.length);
        for (int i = 0; i < Main.anggota.length; i++) {
            if (Main.anggota[i] != null) {
                a = a + " " + Main.anggota[i].getNama();
            }

        }
        area_nama = new JTextArea(a);
        area_nama.setBounds(180, 75, 180, 20);
        this.add(area_nama);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
