/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.util.ArrayList;
/**
 *
 * @author asus
 */
public abstract class Penduduk {
    private String nama;
    private String tempatTanggalLahir;

    public Penduduk() {
    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {
        this.nama = dataNama;
        this.tempatTanggalLahir = dataTempatTanggalLahir;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String dataNama) {
        this.nama = dataNama;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    public void setTempatTanggalLahir(String dataTempatTanggalLahir) {
        this.tempatTanggalLahir = dataTempatTanggalLahir;
    }
    
    public abstract double hitungIuran();

}
